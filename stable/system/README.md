# System

## Perfmon

```bash
\System\File Read Operations/sec
\System\File Write Operations/sec
\System\File Control Operations/sec
\System\File Read Bytes/sec
\System\File Write Bytes/sec
\System\File Control Bytes/sec
\System\Context Switches/sec
\System\System Calls/sec
\System\File Data Operations/sec
\System\System Up Time
\System\Processor Queue Length
\System\Processes
\System\Threads
\System\Alignment Fixups/sec
\System\Exception Dispatches/sec
\System\Floating Emulations/sec
\System\% Registry Quota In Use
```

## WMI

```bash
class Win32_PerfFormattedData_PerfOS_System : Win32_PerfFormattedData
{
  uint32 AlignmentFixupsPerSec;
  string Caption;
  uint32 ContextSwitchesPerSec;
  string Description;
  uint32 ExceptionDispatchesPerSec;
  uint64 FileControlBytesPerSec;
  uint32 FileControlOperationsPerSec;
  uint32 FileDataOperationsPerSec;
  uint64 FileReadBytesPerSec;
  uint32 FileReadOperationsPerSec;
  uint64 FileWriteBytesPerSec;
  uint32 FileWriteOperationsPerSec;
  uint32 FloatingEmulationsPerSec;
  uint64 Frequency_Object;
  uint64 Frequency_PerfTime;
  uint64 Frequency_Sys100NS;
  string Name;
  uint32 PercentRegistryQuotaInUse;
  uint32 Processes;
  uint32 ProcessorQueueLength;
  uint32 SystemCallsPerSec;
  uint64 SystemUpTime;
  uint32 Threads;
  uint64 Timestamp_Object;
  uint64 Timestamp_PerfTime;
  uint64 Timestamp_Sys100NS;
};
```
