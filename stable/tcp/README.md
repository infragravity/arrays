# TCP

## WMI

### Tcpip_TCP

[definition](https://docs.microsoft.com/en-us/previous-versions/aa394341(v=vs.85))

```bash
class Win32_PerfRawData_Tcpip_TCP : Win32_PerfRawData
{
  string Caption;
  uint32 ConnectionFailures;
  uint32 ConnectionsActive;
  uint32 ConnectionsEstablished;
  uint32 ConnectionsPassive;
  uint32 ConnectionsReset;
  string Description;
  uint64 Frequency_Object;
  uint64 Frequency_PerfTime;
  uint64 Frequency_Sys100NS;
  string Name;
  uint32 SegmentsPerSec;
  uint32 SegmentsReceivedPerSec;
  uint32 SegmentsRetransmittedPerSec;
  uint32 SegmentsSentPerSec;
  uint64 Timestamp_Object;
  uint64 Timestamp_PerfTime;
  uint64 Timestamp_Sys100NS;
};
```

### Tcpip_NetworkInterface

[definition](https://docs.microsoft.com/en-us/previous-versions/aa394293(v=vs.85))

```bash
class Win32_PerfFormattedData_Tcpip_NetworkInterface : Win32_PerfFormattedData
{
  uint32 BytesReceivedPerSec;
  uint32 BytesSentPerSec;
  uint64 BytesTotalPerSec;
  string Caption;
  uint32 CurrentBandwidth;
  string Description;
  uint64 Frequency_Object;
  uint64 Frequency_PerfTime;
  uint64 Frequency_Sys100NS;
  string Name;
  uint32 OutputQueueLength;
  uint32 PacketsOutboundDiscarded;
  uint32 PacketsOutboundErrors;
  uint32 PacketsPerSec;
  uint32 PacketsReceivedDiscarded;
  uint32 PacketsReceivedErrors;
  uint32 PacketsReceivedNonUnicastPerSec;
  uint32 PacketsReceivedPerSec;
  uint32 PacketsReceivedUnicastPerSec;
  uint32 PacketsReceivedUnknown;
  uint32 PacketsSentNonUnicastPerSec;
  uint32 PacketsSentPerSec;
  uint32 PacketsSentUnicastPerSec;
  uint64 Timestamp_Object;
  uint64 Timestamp_PerfTime;
  uint64 Timestamp_Sys100NS;
};
```

## Arrays

### Raw

* [Win32_PerfRawData_Tcpip_TCPv4](https://msdn.microsoft.com/en-us/library/aa394341(v=vs.85).aspx) - TODO
* Win32_PerfRawData_Tcpip_NetworkInterface - TODO

### Formatted

* [Win32_PerfFormattedData_Tcpip_TCPv4](https://msdn.microsoft.com/en-us/library/aa394341(v=vs.85).aspx) - TODO
* Win32_PerfFormattedData_Tcpip_NetworkInterface

## See Also

* Performance Counters - [pdh-tcp-sonar.config](/pdh-tcp-sonar.config)