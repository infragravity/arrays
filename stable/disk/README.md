# Disk

```bash
class Win32_PerfRawData_PerfDisk_LogicalDisk : Win32_PerfRawData
{
  uint64 AvgDiskBytesPerRead;
  uint32 AvgDiskBytesPerRead_Base;
  uint64 AvgDiskBytesPerTransfer;
  uint32 AvgDiskBytesPerTransfer_Base;
  uint64 AvgDiskBytesPerWrite;
  uint32 AvgDiskBytesPerWrite_Base;
  uint64 AvgDiskQueueLength; 
  uint64 AvgDiskReadQueueLength; 
  uint32 AvgDiskSecPerRead; 
  uint32 AvgDiskSecPerRead_Base; 
  uint32 AvgDiskSecPerTransfer; 
  uint32 AvgDiskSecPerTransfer_Base; 
  uint32 AvgDiskSecPerWrite; 
  uint32 AvgDiskSecPerWrite_Base; 
  uint64 AvgDiskWriteQueueLength; 
  string Caption; 
  uint32 CurrentDiskQueueLength; 
  string Description; 
  uint64 DiskBytesPerSec; 
  uint64 DiskReadBytesPerSec; 
  uint32 DiskReadsPerSec; 
  uint32 DiskTransfersPerSec; 
  uint64 DiskWriteBytesPerSec; 
  uint32 DiskWritesPerSec; 
  uint32 FreeMegabytes; 
  uint64 Frequency_Object; 
  uint64 Frequency_PerfTime; 
  uint64 Frequency_Sys100NS; 
  string Name; 
  uint64 PercentDiskReadTime; 
  uint64 PercentDiskReadTime_Base; 
  uint64 PercentDiskTime; 
  uint64 PercentDiskTime_Base; 
  uint64 PercentDiskWriteTime; 
  uint64 PercentDiskWriteTime_Base; 
  uint32 PercentFreeSpace; 
  uint32 PercentFreeSpace_Base; 
  uint64 PercentIdleTime; 
  uint64 PercentIdleTime_Base; 
  uint32 SplitIOPerSec; 
  uint64 Timestamp_Object; 
  uint64 Timestamp_PerfTime; 
  uint64 Timestamp_Sys100NS; 
};
```