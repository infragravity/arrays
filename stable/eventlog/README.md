# Windows Event Log

## Inputs

* WMI Adapter

## Outputs

* InfluxDB

## Sample

Select all records from application event log from last 20 seconds:

```bash
select TimeGenerated,Message,EventCode,ComputerName,SourceName,EventType from Win32_NTLogEvent where TimeGenerated > timeshift(20s) and LogFile='Application' and EventType>0
```

InfluxDb supports data de-duplication using timestamp. It is recommended to configure use of ```TimeGenerated``` field to use existing record timestamp as shown below:

```bash
<Queries>
    <add name="ApplicationLog"
        filter="select TimeGenerated,Message,EventCode,ComputerName,SourceName,EventType from Win32_NTLogEvent where TimeGenerated > timeshift(20s) and LogFile='Application' and EventType>0"
        resource="http://schemas.microsoft.com/wbem/wsman/1/wmi/root/cimv2/*"
        namespace="root\cimv2"
        timestamp="TimeGenerated">
        <Tags>
          <add name="ComputerName" value= "ComputerName" />
          <add name="SourceName" value= "SourceName" />
        </Tags>
        <Values>
            <add name="EventCode" value="CimType.UInt16" />
            <add name="EventType" value="CimType.UInt8" />
        </Values>
    </add>
  </Queries>
```

Note that in this example, WMI adapter allows properties of type CimType.String to be omitted in ```Values``` block.

### WMI

#### Win32_NTLogEvent Class
  
This class is used to translate instances from the NT Eventlog.

#### Properties

##### Category - UInt16

Qualifiers: CIMTYPE, Description, DisplayName

Specifies a subcategory for this event. This subcategory is source specific.

##### CategoryString - String

Qualifiers: CIMTYPE, Description, DisplayName

Specifies the translation of the subcategory. The translation is source specific.

##### ComputerName - String

Qualifiers: CIMTYPE, Description, DisplayName, Fixed

The variable-length null-terminated string specifying the name of the computer that generated this event.

##### Data - UInt8 []

Qualifiers: CIMTYPE, Description, DisplayName

The binary data that accompanied the report of the NT event.

##### EventCode - UInt16

Qualifiers: CIMTYPE, Description, DisplayName

This property has the value of the lower 16-bits of the EventIdentifier property. It is present to match the value displayed in the NT Event Viewer. NOTE: Two events from the same source may have the same value for this property but may have different severity and EventIdentifier values

##### EventIdentifier - UInt32

Qualifiers: CIMTYPE, Description, DisplayName, Fixed

Identifies the event. This is specific to the source that generated the event log entry, and is used, together with SourceName, to uniquely identify an NT event type.

##### EventType - UInt8

Qualifiers: CIMTYPE, Description, DisplayName, Fixed, ValueMap, Values

The Type property specifies the type of event.
       Possible Enumeration Values:
       0 - Success
       1 - Error
       2 - Warning
       3 - Information
       4 - Security Audit Success
       5 - Security Audit Failure

##### InsertionStrings - String []

Qualifiers: CIMTYPE, Description, DisplayName

The insertion strings that accompanied the report of the NT event.

##### Logfile - String

Qualifiers: CIMTYPE, Description, DisplayName, key

The name of NT Eventlog logfile. This is used together with the RecordNumber to uniquely identify an instance of this class.

##### Message - String

Qualifiers: CIMTYPE, Description, DisplayName

The event message as it appears in the NT Eventlog. This is a standard message with zero or more insertion strings supplied by the source of the NT event. The insertion strings are inserted into the standard message in a predefined format. If there are no insertion strings or there is a problem inserting the insertion strings, only the standard message will be present in this field.

##### RecordNumber - UInt32

Qualifiers: CIMTYPE, Description, DisplayName, key

Identifies the event within the NT Eventlog logfile. This is specific to the logfile and is used together with the logfile name to uniquely identify an instance of this class.

##### SourceName - String

Qualifiers: CIMTYPE, Description, DisplayName, Fixed

The variable-length null-terminated string specifying the name of the source (application, service, driver, subsystem) that generated the entry. It is used, together with the EventIdentifier, to uniquely identify an NT event type.

##### TimeGenerated - DateTime

Qualifiers: CIMTYPE, Description, DisplayName, Fixed

Specifies the time at which the source generated the event.

##### TimeWritten - DateTime

Qualifiers: CIMTYPE, Description, DisplayName, Fixed

Specifies the time at which the event was written to the logfile.

##### Type - String

Qualifiers: CIMTYPE, Description, DisplayName, Fixed, ValueMap, Values

Specifies the type of event. This is an enumerated string

Possible Enumeration Values:
       0 - Success
       1 - Error
       2 - Warning
       4 - Information
       8 - Audit Success
       16 - Audit Failure

##### User - String

Qualifiers: CIMTYPE, Description, DisplayName

The user name of the logged on user when the event ocurred. If the user name cannot be determined this will be NULL

Search Online Documentation:
[search](http://www.bing.com/search?q=Win32_NTLogEvent+WMI+Class)
