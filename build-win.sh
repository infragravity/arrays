daemon=tcp://10.0.0.145:2375
image=infragravity/sample-coreserver
container=sample-coreserver
docker -H $daemon rmi $image --force
docker -H $daemon build -f Dockerfile -t $image .
docker -H $daemon run -it -d --name $container -p:8556:8000 -p:8050:5000 $image powershell