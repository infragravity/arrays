# Sonar Arrays Catalog

| type | os | description | status |
| ------------- |:-------------:|:------------:|:-----:|
| [system](stable/system/README.md) | Windows | WMI, perfmon | + |
| [.NET Framework](stable/dotnetclr/README.md) | Windows | perfmon | + |
| [eventlog](stable/eventlog/README.md) | Windows | WMI | + |
| [BizTalk](stable/biztalk/README.md) | Windows | WMI, perfmon | - |
| process | Windows | TBA - WMI, perfmon | - |
| service | Windows | TBA - WMI, perfmon | - |
| tcp | Windows | TBA - perfmon | - |
| net | Windows | TBA - perfmon | - |
| os | Windows | TBA - perfmon | - |
| memory | Windows | TBA - perfmon | - |
| logical_disk | Windows | TBA - perfmon | - |
| msmq | Windows | TBA - perfmon | - |
| mssql | Windows | TBA - perfmon | - |
| thermalzone | Windows | TBA - perfmon | - |
| cpu | Windows | TBA - perfmon | - |
| iis | Windows | TBA - perfmon | - |
| dns | Windows | TBA - perfmon | - |
