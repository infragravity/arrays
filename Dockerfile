FROM mcr.microsoft.com/windows/servercore:1809
LABEL Name=infragravity/sonar-samples-webapi Version=0.0.1 
LABEL Maintainer=info@infragravity.com
#use powershell
#SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]
#RUN Invoke-WebRequest https://gitlab.com/infragravity/sonar-docker/raw/0.2.5/releases/sonar-d-0215-win10-x64.zip -OutFile Sonar.zip
#RUN Expand-Archive Sonar.zip -DestinationPath \sonar
#use curl and tar
RUN mkdir -p c:\sonar
RUN curl -L https://gitlab.com/infragravity/sonar-docker/raw/0.2.5/releases/sonar-d-0215-win10-x64.zip -o c:\sonar\Sonar.zip
RUN tar -xvf c:\sonar\Sonar.zip -C c:\sonar
WORKDIR /out
#COPY ./out .
# create content file for IIS
ADD ./index.htm .
# create sonar config files and arrays
ADD ./Sonard.config c:/sonar
ADD ./Sonard.dll.config c:/sonar/out
SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]
RUN Install-WindowsFeature Web-Server
#uncomment to install ASP.NET
#RUN Install-WindowsFeature Web-Asp-Net-45
RUN New-IISSite -Name "AspNetCore" -PhysicalPath c:\out\ -BindingInformation "*:8000:"
RUN Set-WSManInstance WinRM/Config/Service -ValueSet @{AllowUnencrypted=\"true\"}
RUN sc.exe create sonard binpath= c:\sonar\out\Sonard.exe start= auto obj= LocalSystem depend= \"WinRM\"
ENTRYPOINT ["powershell.exe", ""]